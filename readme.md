# icalBuddy

This is a command-line utility that can be used to get lists of events and tasks/to-do's from the macOS calendar database (the same one Calendar.app uses).

Read more at <http://hasseg.org/icalBuddy>.

Compiles and runs successfully on a MacBookPro15,1 running macOS 10.14.3
Mojave.

A fork of https://github.com/ali-rantakari/icalBuddy
